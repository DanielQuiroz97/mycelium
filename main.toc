\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {spanish}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Obtenci\'on de in\'oculos}{5}{chapter.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Materiales}{5}{section.1.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.1}Micelio de hongo}{5}{subsection.1.1.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.2}Canguil}{5}{subsection.1.1.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.3}Fundas de celof\'an}{6}{subsection.1.1.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.4}Cuchara}{6}{subsection.1.1.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.5}Alcohol potable (etanol) al 70\%}{6}{subsection.1.1.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.6}Olla de presi\'on}{6}{subsection.1.1.6}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.7}Mechero/vela}{7}{subsection.1.1.7}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.8}Guantes de latex libre de talco}{7}{subsection.1.1.8}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.9}Mascarilla}{7}{subsection.1.1.9}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Preparaci\'on del \'area de trabajo}{7}{section.1.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.1}Consideraciones}{7}{subsection.1.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.2}Limpieza de superficies}{7}{subsection.1.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Preparaci\'on del sustrato}{8}{section.1.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.1}Hidrataci\'on}{8}{subsection.1.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.2}Esterilizaci\'on}{8}{subsection.1.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.3}Enfundado}{8}{subsection.1.3.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Proceso de inoculaci\'on}{9}{section.1.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4.1}Flameado}{10}{subsection.1.4.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Agregar alcohol}{10}{section*.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Inicio de la llama}{11}{section*.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Enfriamiento}{11}{section*.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4.2}Transferencia de micelio}{11}{subsection.1.4.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5}Almacenamiento}{12}{section.1.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.5.1}almacenamiento}{12}{subsection.1.5.1}% 
\contentsfinish 
